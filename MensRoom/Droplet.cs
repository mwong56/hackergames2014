﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameManagers;
using BananaBoat.GameObjects.GraphicalObjects;

namespace DefendLinkedin
{
	public class Droplet : AbstractSpriteObject
	{
		protected Vector2 speed;
		protected float life;
		protected float newLife;
		protected Color color;

		public Droplet(Vector2 Position, Color color)
		{
			position = Position;
			Random rand = GameManager.GetGlobalState<Random>("RANDOM");
			speed = new Vector2(rand.Next(100) - 50.0f, 0.0f - (100.0f + rand.Next(375)));
			life = Math.Abs(speed.Y) * 3.5f - rand.Next(200);
			newLife = 5000.0f;
			scale = Vector2.One;
			this.color = color;
		}

		public override void Update()
		{
			float dt = GameManager.GetGlobalState<float>("DT");
			if (life > 0.0f)
			{
				life -= 1000.0f * dt;

				speed.Y += 600.0f * dt;

				position += speed * dt;
			}
			else
			{
				if (newLife > 0.0f)
				{
					newLife -= 1000.0f * dt;
					scale += new Vector2(0.5f * dt);
				}
				else
				{
					this.destroy = true;
				}
			}
		}

		public override void Render()
		{
			if (life > 0.0f)
			{
				float angle = BananaBoat.GameHelpers.MathHelper.Angle(Vector2.UnitX, speed);
				RenderManager.DrawQuad("droplet", position, scale, angle, RenderManager.OriginKeys.center, color);
			}
			else
			{
				RenderManager.DrawQuad("pool", position, scale, 0.0f, RenderManager.OriginKeys.center, color * (newLife / 5000.0f));
			}
		}
	}
}
