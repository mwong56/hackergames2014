﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameObjects.GraphicalObjects;

namespace DefendLinkedin
{
	public abstract class AbstractEnemy : AbstractSpriteObject
	{
		public int health {get; set;}

        public int dmg { get; set; }

		public void ApplyDamage(int Damage)
		{
			health -= Damage;
		}
	}
}
