﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameManagers;

namespace DefendLinkedin
{
	public class FastEnemy : AbstractEnemy
	{
		protected Vector2 speed;
		protected float maxSpeed;
		protected float bopPeriod;

        public FastEnemy(Vector2 Position, Vector2 Speed)
		{
			position = Position;
			scale = new Vector2(1.0f);
			maxSpeed = 50.0f * 1.5f;
			speed = Speed;
            health = 1;
            dmg = 1;

			Random rand = GameManager.GetGlobalState<Random>("RANDOM");
			bopPeriod = (float)rand.NextDouble() * MathHelper.TwoPi;
		}

		public override void Update()
		{
            if (health <= 0)
            {
				for (int i = 0; i < 30; i++)
				{
					Droplet droplet = new Droplet(this.position, new Color(0.8f, 0.1f, 0.1f));
					level.RegisterGameObject(droplet);
				}
                Player.totalScore += 200;
                this.destroy = true;
                return;
            }
			float dt = GameManager.GetGlobalState<float>("DT");

			bopPeriod += 32.0f * dt;

			HomeBase homeBase = level.GetLevelState<HomeBase>("home_base");

			if (homeBase != null)
			{
				Vector2 posDif = homeBase.GetPosition() - position;
				posDif.Normalize();
				speed = (speed * 19.0f + posDif * maxSpeed * dt) / 20.0f;
				position += speed;

				if (GameManager.IsCollided(this.position, 16.0f, homeBase.GetPosition(), 32.0f))
				{
					homeBase.ApplyDamage(5);
					ApplyDamage(1000);
				}
			}
		}

		public override void Render()
		{
			RenderManager.DrawQuad("fastEnemy", position + new Vector2(0.0f, Math.Abs(0.0f - (float)Math.Sin(bopPeriod) * 6.0f)), scale, RenderManager.OriginKeys.center, speed.X > 0 ? false : true);
            RenderManager.DrawQuad("blank", position + new Vector2(-12, -24), new Vector2(30.0f * ((float)health / 1.0f), 5), RenderManager.OriginKeys.topLeft, Color.Red);
		}
	}
}
