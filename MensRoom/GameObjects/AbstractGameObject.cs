﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using BananaBoat.GameLevels;

namespace BananaBoat.GameObjects
{
	public abstract class AbstractGameObject
	{
		public int id;
		public AbstractGameLevel level;

		public bool destroy = false;

		public abstract void Update();
		public abstract void Render();
	}
}
