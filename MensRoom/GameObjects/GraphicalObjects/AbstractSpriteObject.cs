﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using BananaBoat.GameManagers;

namespace BananaBoat.GameObjects.GraphicalObjects
{
	public abstract class AbstractSpriteObject : AbstractGameObject
	{
		protected Vector2 position;
		protected float angle;
		protected Vector2 scale;
		protected Color color;
		protected RenderManager.BaseOriginKeys[] origin;

		public Vector2 GetPosition()
		{
			return position;
		}
	}
}
