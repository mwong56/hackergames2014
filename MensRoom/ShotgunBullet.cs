﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameObjects.GraphicalObjects;
using BananaBoat.GameManagers;

namespace DefendLinkedin
{
    public class ShotGunBullet: AbstractAnimatedSpriteObject
    {
        private float movementSpeed;
        private Vector2 direction;
        private float dt;
        private int dmg;
        private int frameCount;

        public ShotGunBullet(Vector2 position, Vector2 direction, float movementSpeed)
		{
            this.position = position;
            this.movementSpeed = movementSpeed;
            this.direction = direction;
            this.direction.Normalize();
            this.direction *= movementSpeed;
            scale = new Vector2(0.2f);
            dmg = 10;
            frameCount = 0;
		}

        public override void Update()
        {
            frameCount++;
            
            if (frameCount >= 15)
            {
                Bullet bullet;
                Bullet bullet1;
                Bullet bullet2;
                Bullet bullet3;
                Bullet bullet4;

                if (direction.Y == 0)
                {
                     bullet = new Bullet(this.position, this.direction, this.movementSpeed);
                     bullet1 = new Bullet(this.position + new Vector2(0, 30), this.direction, this.movementSpeed);
                     bullet2 = new Bullet(this.position + new Vector2(0, 60), this.direction, this.movementSpeed);
                     bullet3 = new Bullet(this.position + new Vector2(0, -30), this.direction, this.movementSpeed);
                     bullet4 = new Bullet(this.position + new Vector2(0, -60), this.direction, this.movementSpeed);
                }
                else
                {
                     bullet = new Bullet(this.position, this.direction, this.movementSpeed);
                     bullet1 = new Bullet(this.position + new Vector2(30, 0), this.direction, this.movementSpeed);
                     bullet2 = new Bullet(this.position + new Vector2(60, 0), this.direction, this.movementSpeed);
                     bullet3 = new Bullet(this.position + new Vector2(-30, 0), this.direction, this.movementSpeed);
                     bullet4 = new Bullet(this.position + new Vector2(-60, 0), this.direction, this.movementSpeed);
                }

                level.RegisterGameObject(bullet);
                level.RegisterGameObject(bullet1);
                level.RegisterGameObject(bullet2);
                level.RegisterGameObject(bullet3);
                level.RegisterGameObject(bullet4);

                this.destroy = true;

                return;
            }
               
            if (!GameManager.IsInScreen(this.position))
            {
                this.destroy = true;
            }

			HomeBase homeBase = level.GetLevelState<HomeBase>("home_base");
            if (homeBase != null && GameManager.IsCollided(this.position, 4.0f, homeBase.GetPosition(), 32.0f))
            {
                this.destroy = true;
            }


            List<BasicEnemy> enemyList = level.GetGameObjectsByType<BasicEnemy>();

            foreach (BasicEnemy enemy in enemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health -= this.dmg;
                    if (enemy.health <= 0)
                    {
                        Player.totalScore += 100;
                        enemy.destroy = true;
                    }
                    this.destroy = true;
                }
            }

            List<TankEnemy> tankEnemyList = level.GetGameObjectsByType<TankEnemy>();

            foreach (TankEnemy enemy in tankEnemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health -= this.dmg;
                    if (enemy.health <= 0)
                    {
                        Player.totalScore += 300;
                        enemy.destroy = true;
                    }
                    this.destroy = true;
                }
            }

            List<FastEnemy> fastEnemyList = level.GetGameObjectsByType<FastEnemy>();

            foreach (FastEnemy enemy in fastEnemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health -= this.dmg;
                    if (enemy.health <= 0)
                    {
                        Player.totalScore += 300;
                        enemy.destroy = true;
                    }
                    this.destroy = true;
                }
            }

            if (GameManager.IsCollided(this.position, 4.0f, level.GetLevelState<HomeBase>("home_base").GetPosition(), 32.0f))
            {
                this.destroy = true;
            }

            this.dt = GameManager.GetGlobalState<float>("DT");
            this.position += this.direction * dt;
           
        }

        public override void Render()
        {
			RenderManager.DrawQuad("blank", position, scale, RenderManager.OriginKeys.center);
        }

    }


}
