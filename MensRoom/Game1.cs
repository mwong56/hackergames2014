﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;

using BananaBoat.GameManagers;
#endregion

namespace DefendLinkedin
{
	/// <summary>
	/// This is the main type for your game
	/// </summary>
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		GameLevel gameLevel;

		public Game1()
			: base()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			GameManager.InitGameManager();
			RenderManager.InitRenderManager(graphics, this.Content, new Vector2(640, 480));
			RenderManager.SetScroll(new Vector2(320, 240));
			InputManager.InitInputManager();

			InputManager.BindInputs("move_left", Keys.Left, Buttons.DPadLeft, PlayerIndex.One);
			InputManager.BindInputs("move_right", Keys.Right, Buttons.DPadRight, PlayerIndex.One);
			InputManager.BindInputs("move_up", Keys.Up, Buttons.DPadUp, PlayerIndex.One);
			InputManager.BindInputs("move_down", Keys.Down, Buttons.DPadDown, PlayerIndex.One);
            InputManager.BindInputs("space_bar", Keys.Space, Buttons.A, PlayerIndex.One);
            InputManager.BindInputs("shot_gun", Keys.G, Buttons.A, PlayerIndex.One);
			InputManager.BindInputs("swing_sword", Keys.F, Buttons.A, PlayerIndex.One);

			gameLevel = new GameLevel();

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			RenderManager.LoadTexture("blank", @"blank");
			RenderManager.LoadTexture("inHeadquarters", @"inHeadquarters");
			RenderManager.LoadTexture("basicEnemy", @"basicEnemy");
			RenderManager.LoadTexture("droplet", @"droplet");
			RenderManager.LoadTexture("pool", @"pool");
			RenderManager.LoadTexture("tankEnemy", @"tank");
			RenderManager.LoadTexture("laserSword", @"laserSword");
			RenderManager.LoadTexture("background", @"background");
			RenderManager.LoadTexture("player", @"player");
			RenderManager.LoadTexture("fastEnemy", @"fastEnemy");
			RenderManager.LoadTexture("strike", @"strike");
			RenderManager.LoadTexture("bullet", @"bullet");

			SpriteFont font = this.Content.Load<SpriteFont>(@"fontStats");
			GameManager.SetGlobalState("font", font);
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			GameManager.UpdateGameManager(gameTime);
			InputManager.UpdateInputManager();

			gameLevel.Update();

			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.Black);

			RenderManager.BeginRender();

			gameLevel.Render();

			RenderManager.EndRender();

			base.Draw(gameTime);
		}

        private void CheckCollisions()
        {
            
        }   
	}
}
