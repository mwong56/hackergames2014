﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using BananaBoat.GameObjects.GraphicalObjects;
using BananaBoat.GameManagers;

namespace DefendLinkedin
{
    public class Player: AbstractAnimatedSpriteObject
    {
        private float movementSpeed;
        private Vector2 msVector;
        private Vector2 persVector;
        private float dt;
        private int frameCount;
        public static int health;
        public static int totalScore { get; set; }

		private bool swingingSword;
		private float swordTimer;
		private float startSwordAngle;
		private float swordAngle;

		protected float bopPeriod;

		protected Dictionary<string, float> timeSincePress;

		public Player()
		{
			position = new Microsoft.Xna.Framework.Vector2(320, 240);
            movementSpeed = 175.0f;
			scale = new Vector2(1.0f);
            msVector = new Vector2(0, 0);
            persVector = new Vector2(0, 0);
            frameCount = 0;
            health = 100;
            totalScore = 0;
			swordTimer = 0.0f;
			startSwordAngle = 0.0f;
			swordAngle = 0.0f;

			Random rand = GameManager.GetGlobalState<Random>("RANDOM");
			bopPeriod = (float)rand.NextDouble() * MathHelper.TwoPi;

			timeSincePress = new Dictionary<String, float>();
			timeSincePress.Add("move_left", 0.0f);
			timeSincePress.Add("move_right", 0.0f);
			timeSincePress.Add("move_up", 0.0f);
			timeSincePress.Add("move_down", 0.0f);
		}

        public override void Update()
        {
            frameCount++;
            this.dt = GameManager.GetGlobalState<float>("DT");
            DoMovement();

			bopPeriod += 24.0f * dt;

            List<BasicEnemy> enemyList = level.GetGameObjectsByType<BasicEnemy>();

            foreach (BasicEnemy enemy in enemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health = 0;
                    Player.health -= enemy.dmg;
                    if (Player.health <= 0)
                    {
                        totalScore += 100;
                        this.destroy = true;
                    }

                }
            }

            List<TankEnemy> tankEnemyList = level.GetGameObjectsByType<TankEnemy>();

            foreach (TankEnemy enemy in tankEnemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health = 0;
                    Player.health -= enemy.dmg;
                    if (Player.health <= 0)
                    {
                        totalScore += 300;
                        this.destroy = true;
                    }

                }
            }

            List<FastEnemy> fastEnemyList = level.GetGameObjectsByType<FastEnemy>();

            foreach (FastEnemy enemy in fastEnemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health = 0;
                    Player.health -= enemy.dmg;
                    if (Player.health <= 0)
                    {
                        totalScore += 200;
                        this.destroy = true;
                    }

                }
            }

            if (frameCount > 6)
            {
                CheckBulletAttack();
                frameCount = 0;
            }
            CheckShotgunBulletAttack();
			CheckSwordAttack();

            if (!msVector.Equals(new Vector2(0,0))) 
            {
                persVector = msVector;
            }
        }

        public override void Render()
        {
			Vector2 bop = new Vector2(0.0f, Math.Abs(0.0f - (float)Math.Sin(bopPeriod) * 6.0f));
			RenderManager.DrawQuad("player", position + ((msVector.X != 0.0f || msVector.Y != 0.0f) ? bop : Vector2.Zero), scale, RenderManager.OriginKeys.center, msVector.X > 0 ? false : true);

			msVector = new Vector2(0, 0);

			if (swingingSword)
			{
				RenderManager.DrawQuad("laserSword", position, new Vector2(1.0f), swordAngle, RenderManager.OriginKeys.left);
			}
            RenderManager.DrawQuad("blank", position + new Vector2(-14, -28), new Vector2(30.0f * ((float)health / 100.0f), 5), RenderManager.OriginKeys.topLeft, Color.Yellow);

			RenderManager.DrawString(GameManager.GetGlobalState<SpriteFont>("font"), totalScore.ToString().PadLeft(5, '0'), new Vector2(270, 16));
        }

        private void DoMovement()
        {
            float dt = GameManager.GetGlobalState<float>("DT");

            if (InputManager.IsInputHeld("move_left") == true && position.X > RenderManager.GetLeftEdge())
            {
                msVector += new Vector2(-movementSpeed, 0);
            }
            if (InputManager.IsInputHeld("move_right") == true && position.X < RenderManager.GetRightEdge())
            {
                msVector += new Vector2(movementSpeed, 0);
            }
            if (InputManager.IsInputHeld("move_up") == true && position.Y > RenderManager.GetTopEdge() + 40)
            {
                msVector += new Vector2(0, -movementSpeed);
            }
            if (InputManager.IsInputHeld("move_down") == true && position.Y < RenderManager.GetBottomEdge())
            {
                msVector += new Vector2(0, movementSpeed);
            }

            position += msVector * dt;

			HomeBase homeBase = level.GetLevelState<HomeBase>("home_base");

			if (homeBase != null && GameManager.IsCollided(this.position, 16.0f, homeBase.GetPosition(), 32.0f))
			{
				float angle = BananaBoat.GameHelpers.MathHelper.Angle(homeBase.GetPosition(), this.position);
				this.position = homeBase.GetPosition() + new Vector2((float)Math.Cos(angle) * 48.0f, (float)Math.Sin(angle) * 48.0f);
			}

			timeSincePress["move_left"] += 1000.0f * dt;
			timeSincePress["move_right"] += 1000.0f * dt;
			timeSincePress["move_down"] += 1000.0f * dt;
			timeSincePress["move_up"] += 1000.0f * dt;

			bool teleported = false;
			Vector2 oldPos = this.position;

			if (InputManager.IsInputHit("move_left"))
			{
				if (timeSincePress["move_left"] > 0.0f && timeSincePress["move_left"] < 250.0f)
				{
					this.position.X -= 100.0f;
					teleported = true;
				}

				timeSincePress["move_left"] = 0.0f;
			}
			if (InputManager.IsInputHit("move_right"))
			{
				if (timeSincePress["move_right"] > 0.0f && timeSincePress["move_right"] < 250.0f)
				{
					this.position.X += 100.0f;
					teleported = true;
				}

				timeSincePress["move_right"] = 0.0f;
			}
			if (InputManager.IsInputHit("move_up"))
			{
				if (timeSincePress["move_up"] > 0.0f && timeSincePress["move_up"] < 250.0f)
				{
					this.position.Y -= 100.0f;
					teleported = true;
				}

				timeSincePress["move_up"] = 0.0f;
			}
			if (InputManager.IsInputHit("move_down"))
			{
				if (timeSincePress["move_down"] > 0.0f && timeSincePress["move_down"] < 250.0f)
				{
					this.position.Y += 100.0f;
					teleported = true;
				}

				timeSincePress["move_down"] = 0.0f;
			}

			if (teleported)
			{
				for (int i = 0; i < 30; i++)
				{
					Droplet droplet = new Droplet(oldPos, new Color(1.0f, 1.0f, 0.5f));
					level.RegisterGameObject(droplet);
				}

				Strike strike = new Strike(250.0f, new Color(1.0f, 1.0f, 0.6f), this.position);
				level.RegisterGameObject(strike);
			}
        }

        private void CheckBulletAttack()
        {
            if (InputManager.IsInputHeld("space_bar") == true)
            {
                Bullet bullet;
                if (msVector.Equals(new Vector2(0, 0)))
                {
                    bullet = new Bullet(this.position, persVector, 600.0f);
                }
                else
                {
                    bullet = new Bullet(this.position, msVector, 600.0f);
                }
                level.RegisterGameObject(bullet);
            }
        }

        private void CheckShotgunBulletAttack()
        {
            if (InputManager.IsInputHeld("shot_gun") == true)
            {
                ShotGunBullet bullet;
                if (msVector.Equals(new Vector2(0, 0)))
                {
                    bullet = new ShotGunBullet(this.position, persVector, 600.0f);
                }
                else
                {
                    bullet = new ShotGunBullet(this.position, msVector, 600.0f);
                }
                level.RegisterGameObject(bullet);
            }
        }

		private void CheckSwordAttack()
		{
			if (!swingingSword)
			{
                if (InputManager.IsInputHeld("swing_sword"))
				{
					swordTimer = 100.0f;
					startSwordAngle = (float)BananaBoat.GameHelpers.MathHelper.Angle(Vector2.UnitX, persVector);
					swordAngle = startSwordAngle - MathHelper.PiOver2;
					swingingSword = true;
				}
			}
			else
			{
				swordTimer -= 1000.0f * dt;
				swordAngle = startSwordAngle - MathHelper.PiOver2 + MathHelper.Pi * ((100.0f - swordTimer) / 100.0f);

				List<BasicEnemy> enemyList = level.GetGameObjectsByType<BasicEnemy>();

				foreach (BasicEnemy enemy in enemyList)
				{
					float enemyAngle = BananaBoat.GameHelpers.MathHelper.Angle(this.position, enemy.GetPosition());
					enemyAngle = MathHelper.WrapAngle(enemyAngle);
					float wrappedStartAngle = MathHelper.WrapAngle(startSwordAngle);

					if (Math.Abs(wrappedStartAngle - enemyAngle) < MathHelper.PiOver2)
					{
						if (GameManager.IsCollided(this.position, 64.0f, enemy.GetPosition(), 16.0f))
						{
							enemy.ApplyDamage(10);
							Strike strike = new Strike(250.0f, new Color(1.0f, 1.0f, 0.6f), (this.position + enemy.GetPosition()) / 2.0f);
							level.RegisterGameObject(strike);
						}
					}
				}

                List<TankEnemy> tankEnemyList = level.GetGameObjectsByType<TankEnemy>();

                foreach (TankEnemy enemy in tankEnemyList)
                {
                    float enemyAngle = BananaBoat.GameHelpers.MathHelper.Angle(this.position, enemy.GetPosition());
                    enemyAngle = MathHelper.WrapAngle(enemyAngle);
                    float wrappedStartAngle = MathHelper.WrapAngle(startSwordAngle);

                    if (Math.Abs(wrappedStartAngle - enemyAngle) < MathHelper.PiOver2)
                    {
                        if (GameManager.IsCollided(this.position, 96.0f, enemy.GetPosition(), 16.0f))
                        {
                            enemy.ApplyDamage(10);
							Strike strike = new Strike(250.0f, new Color(1.0f, 1.0f, 0.6f) * 0.5f, (this.position + enemy.GetPosition()) / 2.0f);
							level.RegisterGameObject(strike);
                        }
                    }
                }

                List<FastEnemy> fastEnemyList = level.GetGameObjectsByType<FastEnemy>();

                foreach (FastEnemy enemy in fastEnemyList)
                {
                    float enemyAngle = BananaBoat.GameHelpers.MathHelper.Angle(this.position, enemy.GetPosition());
                    enemyAngle = MathHelper.WrapAngle(enemyAngle);
                    float wrappedStartAngle = MathHelper.WrapAngle(startSwordAngle);

					if (Math.Abs(wrappedStartAngle - enemyAngle) < MathHelper.PiOver2)
                    {
                        if (GameManager.IsCollided(this.position, 64.0f, enemy.GetPosition(), 16.0f))
                        {
                            enemy.ApplyDamage(10);
							Strike strike = new Strike(250.0f, new Color(1.0f, 1.0f, 0.6f), (this.position + enemy.GetPosition()) / 2.0f);
							level.RegisterGameObject(strike);
                        }
                    }
                }

				if (swordTimer <= 0.0f)
				{
					swingingSword = false;
				}
			}
		}
    }
}
