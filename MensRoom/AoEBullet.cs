﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameObjects.GraphicalObjects;
using BananaBoat.GameManagers;

namespace DefendLinkedin
{
    public class AoEBullet: AbstractAnimatedSpriteObject
    {
        private float movementSpeed;
        private Vector2 direction;
        private float dt;
        private int dmg;

        public AoEBullet(Vector2 position, Vector2 direction, float movementSpeed)
		{
            this.position = position;
            this.movementSpeed = movementSpeed;
            this.direction = direction;
            this.direction.Normalize();
            this.direction *= movementSpeed;
            scale = new Vector2(0.2f);
            dmg = 10;
		}

        public override void Update()
        {              
            if (!GameManager.IsInScreen(this.position))
            {
                this.destroy = true;
            }

			HomeBase homeBase = level.GetLevelState<HomeBase>("home_base");
            if (homeBase != null && GameManager.IsCollided(this.position, 4.0f, homeBase.GetPosition(), 32.0f))
            {
                this.destroy = true;
            }


            List<BasicEnemy> enemyList = level.GetGameObjectsByType<BasicEnemy>();

            foreach (BasicEnemy enemy in enemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health -= this.dmg;
                    if (enemy.health <= 0)
                    {
                        enemy.destroy = true;
                        Bullet bullet = new Bullet(this.position, this.direction, this.movementSpeed);
                        Bullet bullet1 = new Bullet(this.position + new Vector2(0, 30), this.direction, this.movementSpeed);
                        Bullet bullet2 = new Bullet(this.position + new Vector2(0, 60), this.direction, this.movementSpeed);
                        Bullet bullet3 = new Bullet(this.position + new Vector2(0, -30), this.direction, this.movementSpeed);
                        Bullet bullet4 = new Bullet(this.position + new Vector2(0, -60), this.direction, this.movementSpeed);
                        Bullet bullet5 = new Bullet(this.position + new Vector2(30, 0), -this.direction, this.movementSpeed);
                        Bullet bullet6 = new Bullet(this.position + new Vector2(60, 0), -this.direction, this.movementSpeed);
                        Bullet bullet7 = new Bullet(this.position + new Vector2(-30, 0), -this.direction, this.movementSpeed);
                        Bullet bullet8 = new Bullet(this.position + new Vector2(-60, 0), -this.direction, this.movementSpeed);
                        level.RegisterGameObject(bullet);
                        level.RegisterGameObject(bullet1);
                        level.RegisterGameObject(bullet2);
                        level.RegisterGameObject(bullet3);
                        level.RegisterGameObject(bullet4);
                        level.RegisterGameObject(bullet5);
                        level.RegisterGameObject(bullet6);
                        level.RegisterGameObject(bullet7);
                        level.RegisterGameObject(bullet8);
                    }
                    this.destroy = true;
                }
            }

            if (GameManager.IsCollided(this.position, 4.0f, level.GetLevelState<HomeBase>("home_base").GetPosition(), 32.0f))
            {
                this.destroy = true;
            }

            this.dt = GameManager.GetGlobalState<float>("DT");
            this.position += this.direction * dt;
           
        }

        public override void Render()
        {
			RenderManager.DrawQuad("blank", position, scale, RenderManager.OriginKeys.center);
        }

    }


}
