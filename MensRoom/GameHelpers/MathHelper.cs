﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace BananaBoat.GameHelpers
{
	public class MathHelper
	{
		public static float Angle(Vector2 V1, Vector2 V2)
		{
			return (float)Math.Atan2(V2.Y - V1.Y, V2.X - V1.X);
		}
	}
}
