﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using BananaBoat.GameManagers;
using BananaBoat.GameObjects;

namespace BananaBoat.GameLevels
{
	public abstract class AbstractGameLevel
	{
		protected Dictionary<string, object> levelStates;

		protected List<AbstractGameObject> objects;
		protected Dictionary<Type, List<AbstractGameObject>> objectsByType;

		public void SetLevelState(string Key, object Value)
		{
			if (levelStates.ContainsKey(Key))
			{
				levelStates[Key] = Value;
			}
			else
			{
				levelStates.Add(Key, Value);
			}
		}

		public T GetLevelState<T>(string Key)
		{
			if (!levelStates.ContainsKey(Key))
			{
				return default(T);
			}
			return (T)levelStates[Key];
		}

		public void RemoveLevelState(string Key)
		{
			levelStates.Remove(Key);
		}

		public virtual void InitLevel()
		{
			levelStates = new Dictionary<string, object>();
			objects = new List<AbstractGameObject>();
			objectsByType = new Dictionary<Type, List<AbstractGameObject>>();
		}

		public virtual bool RegisterGameObject(AbstractGameObject GameObject)
		{
			GameObject.id = GameManagers.GameManager.GetNextObjectId();
			GameObject.level = this;

			objects.Add(GameObject);

			Type objType = GameObject.GetType();

			if (!objectsByType.ContainsKey(objType))
			{
				objectsByType.Add(objType, new List<AbstractGameObject>());
			}

			objectsByType[objType].Add(GameObject);

			return true;
		}

		public virtual T GetGameObjectById<T>(int Id) where T : AbstractGameObject
		{
			return (T)objects.Find(ago => ago.id == Id );
		}

		public virtual List<T> GetGameObjectsByType<T>() where T : AbstractGameObject
		{
			if (objectsByType.ContainsKey(typeof(T)))
			{
				return objectsByType[typeof(T)].Cast<T>().ToList();
			}

			return new List<T>();
		}

		public virtual void Update()
		{
			for (int i = 0; i < objects.Count; )
			{
				objects[i].Update();

				if (objects[i].destroy)
				{
					Type objType = objects[i].GetType();
					objectsByType[objType].Remove(objects[i]);

					objects.RemoveAt(i);
				}
				else
				{
					i++;
				}
			}
		}

		public virtual void Render()
		{
			foreach (AbstractGameObject ago in objects)
			{
				ago.Render();
			}
		}
	}
}
