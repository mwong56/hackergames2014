﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameObjects;
using BananaBoat.GameManagers;

namespace DefendLinkedin
{
	public class EnemySpawner : AbstractGameObject
	{
		protected List<Vector2> spawnPoints;
		protected int currentSpawnPoint;

		protected int waveCount;
		protected int maxWaveCount;
		protected float waveTimer;
		protected float spawnTimer;

		public EnemySpawner()
		{
			spawnPoints = new List<Vector2>();

			for (int i = 0; i < 15; i++)
			{
				spawnPoints.Add(new Vector2(320 + 300 * (float)Math.Cos((double)i / 15.0 * Math.PI * 2.0), 240 + 220 * (float)Math.Sin((double)i / 15.0 * Math.PI * 2.0)));
			}
			currentSpawnPoint = 0;

			waveCount = 0;
			maxWaveCount = 2;
			waveTimer = 5000.0f;
			spawnTimer = 0.0f;
		}

		public override void Update()
		{
			HomeBase homeBase = level.GetLevelState<HomeBase>("home_base");

			if (homeBase != null)
			{
				Random rand = GameManager.GetGlobalState<Random>("RANDOM");
				float dt = GameManager.GetGlobalState<float>("DT");

				waveTimer -= 1000.0f * dt;

				if (waveTimer <= 0.0f)
				{
					if (waveCount == 0)
					{
						waveTimer = 5000.0f;
						waveCount = maxWaveCount;
						maxWaveCount++;
						spawnTimer = 250.0f;
					}
				}

				if (waveCount > 0)
				{
					spawnTimer -= 1000.0f * dt;

					if (spawnTimer <= 0.0f)
					{
						waveCount--;
						spawnTimer = 250.0f;

						Vector2 enemySpeed = homeBase.GetPosition() - spawnPoints[currentSpawnPoint];
						enemySpeed.X += rand.Next(400) - 200;
						enemySpeed.Y += rand.Next(400) - 200;
						enemySpeed.Normalize();
                        int random = rand.Next(11);
                        if (random < 6)
                        {
                            BasicEnemy basicEnemy = new BasicEnemy(spawnPoints[currentSpawnPoint], enemySpeed);
                            level.RegisterGameObject(basicEnemy);
                        }
                        else if (random >= 6 && random < 8)
                        {
                            TankEnemy tankEnemy = new TankEnemy(spawnPoints[currentSpawnPoint], enemySpeed);
                            level.RegisterGameObject(tankEnemy);
                        }
                        else if (random > 8)
                        {
                            FastEnemy fastEnemy = new FastEnemy(spawnPoints[currentSpawnPoint], enemySpeed);
                            level.RegisterGameObject(fastEnemy);
                        }

						if (waveCount == 0)
						{
							currentSpawnPoint = rand.Next(spawnPoints.Count);
						}
					}
				}
			}
		}

		public override void Render()
		{
		}
	}
}
