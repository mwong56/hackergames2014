﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameObjects.GraphicalObjects;
using BananaBoat.GameManagers;

namespace DefendLinkedin
{
	public class HomeBase : AbstractSpriteObject
	{
		public static int health;

		public HomeBase()
		{
			position = new Vector2(320, 240);
			scale = Vector2.One;
			health = 100;
		}

		public void ApplyDamage(int DamageAmount)
		{
			health -= DamageAmount;
		}

		public override void Update()
		{
			if (health <= 0)
			{
				destroy = true;
				level.RemoveLevelState("home_base");
			}
		}

		public override void Render()
		{
			RenderManager.DrawQuad("inHeadquarters", position, scale, RenderManager.OriginKeys.center);
            RenderManager.DrawQuad("blank", position + new Vector2(-28, -50), new Vector2(60.0f * ((float)health / 100.0f), 5), RenderManager.OriginKeys.topLeft, Color.Yellow);
		}
	}
}
