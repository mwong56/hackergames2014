﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameObjects.GraphicalObjects;
using BananaBoat.GameManagers;

namespace DefendLinkedin
{
    public class Bullet: AbstractAnimatedSpriteObject
    {
        private float movementSpeed;
        private Vector2 direction;
        private float dt;
        private int dmg;

        public Bullet(Vector2 position, Vector2 direction, float movementSpeed)
		{
            this.position = position;
            this.movementSpeed = movementSpeed;
            this.direction = direction;
            this.direction.Normalize();
            this.direction *= movementSpeed;
            scale = new Vector2(0.8f);
            dmg = 5;
		}

        public override void Update()
        {
            if (!GameManager.IsInScreen(this.position))
            {
                this.destroy = true;
            }

			HomeBase homeBase = level.GetLevelState<HomeBase>("home_base");
            if (homeBase != null && GameManager.IsCollided(this.position, 4.0f, homeBase.GetPosition(), 32.0f))
            {
                this.destroy = true;
            }


            List<BasicEnemy> basicEnemyList = level.GetGameObjectsByType<BasicEnemy>();

            foreach (BasicEnemy enemy in basicEnemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health -= this.dmg;
                    if (enemy.health <= 0)
                    {
                        Player.totalScore += 100;
                        enemy.destroy = true;
                    }
					Strike strike = new Strike(250.0f, new Color(1.0f, 1.0f, 0.6f), (this.position + enemy.GetPosition()) / 2.0f);
					level.RegisterGameObject(strike);
                    this.destroy = true;
                }
            }


            List<TankEnemy> tankEnemyList = level.GetGameObjectsByType<TankEnemy>();

            foreach (TankEnemy enemy in tankEnemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health -= this.dmg;
                    if (enemy.health <= 0)
                    {
                        Player.totalScore += 300;
                        enemy.destroy = true;
                    }
					Strike strike = new Strike(250.0f, new Color(1.0f, 1.0f, 0.6f), (this.position + enemy.GetPosition()) / 2.0f);
					level.RegisterGameObject(strike);
                    this.destroy = true;
                }
            }

            List<FastEnemy> fastEnemyList = level.GetGameObjectsByType<FastEnemy>();

            foreach (FastEnemy enemy in fastEnemyList)
            {
                if (GameManager.IsCollided(this.position, 4.0f, enemy.GetPosition(), 16.0f))
                {
                    enemy.health -= this.dmg;
                    if (enemy.health <= 0)
                    {
                        Player.totalScore += 200;
                        enemy.destroy = true;
                    }
					Strike strike = new Strike(250.0f, new Color(1.0f, 1.0f, 0.6f), (this.position + enemy.GetPosition()) / 2.0f);
					level.RegisterGameObject(strike);
                    this.destroy = true;
                }
            }

            if (GameManager.IsCollided(this.position, 4.0f, level.GetLevelState<HomeBase>("home_base").GetPosition(), 32.0f))
            {
                this.destroy = true;
            }

            this.dt = GameManager.GetGlobalState<float>("DT");
            this.position += this.direction * dt;
           
        }

        public override void Render()
        {
			RenderManager.DrawQuad("bullet", position, scale, RenderManager.OriginKeys.center);
        }

    }


}
