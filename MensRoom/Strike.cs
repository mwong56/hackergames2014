﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using BananaBoat.GameObjects.GraphicalObjects;
using BananaBoat.GameManagers;

namespace DefendLinkedin
{
	public class Strike : AbstractSpriteObject
	{
		protected float life;
		protected float maxLife;

		public Strike(float Life, Color color, Vector2 Position)
		{
			life = Life;
			maxLife = Life;
			this.color = color;
			position = Position;
			scale = Vector2.One;
		}

		public override void Update()
		{
			float dt = GameManager.GetGlobalState<float>("DT");
			life -= 1000.0f * dt;

			if (life <= 0.0f)
			{
				destroy = true;
			}
		}

		public override void Render()
		{
			RenderManager.DrawQuad("strike", position, scale, 0.0f, RenderManager.OriginKeys.center, color * (life / maxLife));
		}
	}
}
