﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace BananaBoat.GameManagers
{
	public class GameManager
	{
		private static Dictionary<string, object> globalStates;
		private static int nextObjectId;

		public static void InitGameManager()
		{
			globalStates = new Dictionary<string, object>();
			globalStates.Add("RANDOM", new Random());
			nextObjectId = 0;
		}

		public static void SetGlobalState(string Key, object Value)
		{
			if (globalStates.ContainsKey(Key))
			{
				globalStates[Key] = Value;
			}
			else
			{
				globalStates.Add(Key, Value);
			}
		}

		public static T GetGlobalState<T>(string Key)
		{
			return (T)globalStates[Key];
		}

		public static int GetNextObjectId()
		{
			return nextObjectId++;
		}

        public static bool IsCollided(Vector2 position1, float radius1, Vector2 position2, float radius2)
        {
            if (Vector2.Distance(position1, position2) <= radius1 + radius2)
            {
                return true;
            }

            return false;
        }

        public static bool IsInScreen(Vector2 position)
        {
            if (position.X < RenderManager.GetLeftEdge() || position.X > RenderManager.GetRightEdge())
            {
                return false;
            }
            if (position.Y < RenderManager.GetTopEdge() || position.Y > RenderManager.GetBottomEdge())
            {
                return false;
            }
            return true;
        }

		public static void UpdateGameManager(GameTime gameTime)
		{
			CalculateDeltaTime(gameTime);
		}

		private static void CalculateDeltaTime(GameTime gameTime)
		{
			float elapsedTime = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
			float dt = elapsedTime / 1000.0f;
			SetGlobalState("DT", dt);
		}

        
	}
}
