﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using BananaBoat.GameChunks;

namespace BananaBoat.GameManagers
{
	public class RenderManager
	{
		public enum BaseOriginKeys
		{
			top,
			left,
			right,
			bottom,
			center
		};

		public struct OriginKeys
		{
			public static BaseOriginKeys[] top = { BaseOriginKeys.top };
			public static BaseOriginKeys[] bottom = { BaseOriginKeys.bottom };
			public static BaseOriginKeys[] left = { BaseOriginKeys.left };
			public static BaseOriginKeys[] right = { BaseOriginKeys.right };
			public static BaseOriginKeys[] center = { BaseOriginKeys.center };
			public static BaseOriginKeys[] topLeft = { BaseOriginKeys.top, BaseOriginKeys.left };
			public static BaseOriginKeys[] topRight = { BaseOriginKeys.top, BaseOriginKeys.right };
			public static BaseOriginKeys[] botLeft = { BaseOriginKeys.bottom, BaseOriginKeys.left };
			public static BaseOriginKeys[] botRight = { BaseOriginKeys.bottom, BaseOriginKeys.right };
		}

		private static ContentManager cm;
		private static GraphicsDeviceManager gd;
		private static SpriteBatch spriteBatch;
		private static Dictionary<string, Texture2D> textures;
		private static Dictionary<string, Animation> animations;

		private static Vector2 targetWindowSize;
		private static float targetScale;

		private static Vector2 scroll;

		public static void InitRenderManager(GraphicsDeviceManager Gd, ContentManager Cm, Vector2 TargetWindowSize)
		{
			gd = Gd;
			cm = Cm;
			textures = new Dictionary<string, Texture2D>();
			animations = new Dictionary<string, Animation>();

			gd.PreferredBackBufferWidth = (int)TargetWindowSize.X;
			gd.PreferredBackBufferHeight = (int)TargetWindowSize.Y;
			gd.CreateDevice();

			spriteBatch = new SpriteBatch(gd.GraphicsDevice);

			targetWindowSize = TargetWindowSize;
			SetScroll(Vector2.Zero);
			RefreshTargetScale();
		}

		public static void RefreshTargetScale()
		{
			targetScale = Math.Min((float)gd.GraphicsDevice.PresentationParameters.BackBufferWidth / targetWindowSize.X, (float)gd.GraphicsDevice.PresentationParameters.BackBufferHeight / targetWindowSize.Y);
		}

		public static void LoadTexture(string TextureKey, string TexturePath)
		{
			if (null != cm)
			{
				textures.Add(TextureKey, cm.Load<Texture2D>(TexturePath));
			}
		}

		public static void LoadAnimations(string AnimationsPath)
		{
			List<Animation> tempAnimations = Animation.LoadAnimationsFromXml(AnimationsPath);
			foreach (Animation a in tempAnimations)
			{
				animations.Add(a.name, a);
			}
		}

		public static Texture2D GetTexture(string TextureKey)
		{
			return textures[TextureKey];
		}

		public static Animation GetAnimation(string AnimationKey)
		{
			return animations[AnimationKey];
		}

		public static void SetScroll(Vector2 Scroll)
		{
			scroll.X = Scroll.X - targetWindowSize.X / 2.0f;
			scroll.Y = Scroll.Y - targetWindowSize.Y / 2.0f;
		}

		public static Vector2 GetScroll()
		{
			return scroll + new Vector2(targetWindowSize.X / 2.0f, targetWindowSize.Y / 2.0f);
		}

        public static float GetTopEdge()
        {
            return GetScroll().Y - targetWindowSize.Y / 2.0f;
        }

        public static float GetBottomEdge()
        {
			return GetScroll().Y + targetWindowSize.Y / 2.0f;
        }

        public static float GetLeftEdge()
        {
			return GetScroll().X - targetWindowSize.X / 2.0f;
        }

        public static float GetRightEdge()
        {
			return GetScroll().X + targetWindowSize.X / 2.0f;
        }
    
		private static Vector2 CalculateOrigin(Texture2D Texture, BaseOriginKeys[] Origin)
		{
			Vector2 origin = new Vector2(Texture.Width / 2.0f, Texture.Height / 2.0f);

			if (Origin.Contains(BaseOriginKeys.center))
			{
				origin.X = Texture.Width / 2.0f;
				origin.Y = Texture.Height / 2.0f;
			}
			if (Origin.Contains(BaseOriginKeys.top))
			{
				origin.Y = 0;
			}
			if (Origin.Contains(BaseOriginKeys.bottom))
			{
				origin.Y = Texture.Height;
			}
			if (Origin.Contains(BaseOriginKeys.left))
			{
				origin.X = 0;
			}
			if (Origin.Contains(BaseOriginKeys.right))
			{
				origin.X = Texture.Width;
			}

			return origin;
		}

		private static Vector2 CalculateOrigin(Rectangle SourceRect, BaseOriginKeys[] Origin)
		{
			Vector2 origin = Vector2.Zero;

			if (Origin.Contains(BaseOriginKeys.top))
			{
				origin.Y = 0;
			}
			if (Origin.Contains(BaseOriginKeys.bottom))
			{
				origin.Y = SourceRect.Height;
			}
			if (Origin.Contains(BaseOriginKeys.left))
			{
				origin.X = 0;
			}
			if (Origin.Contains(BaseOriginKeys.right))
			{
				origin.X = SourceRect.Width;
			}
			if (Origin.Contains(BaseOriginKeys.center))
			{
				origin.X = SourceRect.Width / 2.0f;
				origin.Y = SourceRect.Height / 2.0f;
			}

			return origin;
		}

		public static void BeginRender()
		{
			spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
		}

		public static void EndRender()
		{
			spriteBatch.End();
		}

		public static void DrawQuad(string TextureKey, Vector2 Position, Vector2 Scale, BaseOriginKeys[] Origin)
		{
			Texture2D texture = GetTexture(TextureKey);
			Vector2 origin = CalculateOrigin(texture, Origin);

			spriteBatch.Draw(
				texture, 
				position: (Position - scroll) * targetScale, 
				sourceRectangle: new Rectangle(0, 0, texture.Width, texture.Height), 
				color: Color.White, 
				scale: Scale * targetScale, 
				origin: origin);
		}

		public static void DrawQuad(string TextureKey, Vector2 Position, Vector2 Scale, BaseOriginKeys[] Origin, bool Flip)
		{
			Texture2D texture = GetTexture(TextureKey);
			Vector2 origin = CalculateOrigin(texture, Origin);

			spriteBatch.Draw(
				texture,
				position: (Position - scroll) * targetScale,
				sourceRectangle: new Rectangle(0, 0, texture.Width, texture.Height),
				color: Color.White,
				rotation: 0.0f,
				origin: origin,
				scale: Scale * targetScale,
				effect: Flip ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
				depth: 1.0f);
		}

        public static void DrawQuad(string TextureKey, Vector2 Position, Vector2 Size, BaseOriginKeys[] Origin, Color color)
        {
            Texture2D texture = GetTexture(TextureKey);
            Vector2 origin = CalculateOrigin(texture, Origin);

            spriteBatch.Draw(
                texture, 
                new Rectangle((int)Position.X, (int)Position.Y, (int)Size.X, (int)Size.Y), 
                new Rectangle(0, 0, texture.Width, texture.Height), 
                color, 
                0.0f, 
                origin, 
                SpriteEffects.None, 
                1.0f);
        }


		public static void DrawQuad(string TextureKey, Vector2 Position, Vector2 Scale, float Angle, BaseOriginKeys[] Origin)
		{
			Texture2D texture = GetTexture(TextureKey);
			Vector2 origin = CalculateOrigin(texture, Origin);

			spriteBatch.Draw(
				texture,
				position: (Position - scroll) * targetScale,
				sourceRectangle: new Rectangle(0, 0, texture.Width, texture.Height),
				color: Color.White,
				scale: Scale * targetScale,
				origin: origin,
				rotation: Angle);
		}

		public static void DrawQuad(string TextureKey, Vector2 Position, Vector2 Scale, float Angle, BaseOriginKeys[] Origin, Color color)
		{
			Texture2D texture = GetTexture(TextureKey);
			Vector2 origin = CalculateOrigin(texture, Origin);

			spriteBatch.Draw(
				texture,
				position: (Position - scroll) * targetScale,
				sourceRectangle: new Rectangle(0, 0, texture.Width, texture.Height),
				color: color,
				scale: Scale * targetScale,
				origin: origin,
				rotation: Angle);
		}

		public static void DrawQuad(string TextureKey, Vector2 Position, Rectangle SourceRect, Vector2 Scale, BaseOriginKeys[] Origin)
		{
			Texture2D texture = GetTexture(TextureKey);
			Vector2 origin = CalculateOrigin(SourceRect, Origin);

			spriteBatch.Draw(
				texture,
				position: (Position - scroll) * targetScale,
				sourceRectangle: SourceRect,
				color: Color.White,
				scale: Scale * targetScale,
				origin: origin);
		}

		public static void DrawString(SpriteFont Font, String Text, Vector2 Position)
		{
			spriteBatch.DrawString(Font, Text, Position, Color.White);
		}
	}
}
