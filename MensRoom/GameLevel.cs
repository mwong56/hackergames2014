﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using BananaBoat.GameLevels;
using BananaBoat.GameManagers;

namespace DefendLinkedin  
{
    public class GameLevel: AbstractGameLevel
    {
		protected Player player;
		protected HomeBase homeBase;

		public GameLevel()
		{
			InitLevel();

			player = new Player();
			RegisterGameObject(player);

			homeBase = new HomeBase();
			RegisterGameObject(homeBase);
			SetLevelState("home_base", homeBase);

			EnemySpawner enemySpawner = new EnemySpawner();
			RegisterGameObject(enemySpawner);
		}

		public override void Update()
		{
            if (Player.health > 0 && HomeBase.health > 0)
            {
                base.Update();
            }
		}

		public override void Render()
		{
			RenderManager.DrawQuad("background", Vector2.Zero, Vector2.One, RenderManager.OriginKeys.topLeft);
            if (Player.health <= 0)
            {
                RenderManager.DrawString(GameManager.GetGlobalState<SpriteFont>("font"), "GAME OVER TRASH", new Vector2(160, 180));
                RenderManager.DrawString(GameManager.GetGlobalState<SpriteFont>("font"), "Score: " + Player.totalScore, new Vector2(230, 220));
            }

            else if (HomeBase.health <= 0)
            {
                RenderManager.DrawString(GameManager.GetGlobalState<SpriteFont>("font"), "GAME OVER TRASH", new Vector2(160, 180));
                RenderManager.DrawString(GameManager.GetGlobalState<SpriteFont>("font"), "Score: " + Player.totalScore, new Vector2(230, 220));
            }
            else
            {
                base.Render();
            }
		}
    }
}
